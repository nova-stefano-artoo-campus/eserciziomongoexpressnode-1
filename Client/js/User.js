var Users = (function() {
  var createUser = function(nuovo) {
    return $.ajax({
      url: "http://localhost:3000/users",
      method: "POST",
      contentType: "application/json",
      dataType: "json",
      data: JSON.stringify(nuovo)
    }).then(function(response) {
      console.log(response);
    }).catch(function(err) {
      console.log(err);
    });
  }

  var getUsers = function() {
    return $.ajax({
      url: "http://localhost:3000/users",
      method: "GET",
      contentType: "application/json",
      dataType: "json"
    });
  }

  var deleteUser = function(id) {
    return $.ajax({
      url: "http://localhost:3000/users/id/" + id,
      method: "DELETE",
      contentType: "application/json",
      dataType: "json"
    });
  }

  var detailUser = function(id) {
    return $.ajax({
      url: "http://localhost:3000/users/id/" + id,
      method: "GET",
      contentType: "application/json",
      dataType: "json"
    });
  }

  var updateUser = function(id, user) {
    return $.ajax({
      url: "http://localhost:3000/users/id/" + id,
      method: "PUT",
      contentType: "application/json",
      dataType: "json",
      data: JSON.stringify(user)
    });
  }

  var cercaUsers = function(nome) {
    return $.ajax({
      url: "http://localhost:3000/users/cerca/?cerca=" + nome,
      method: "GET",
      contentType: "application/json",
      dataType: "json"
    });
  }

  return {
    getUsers: getUsers,
    createUser: createUser,
    detailUser: detailUser,
    deleteUser: deleteUser,
    updateUser: updateUser,
    cercaUsers: cercaUsers
  }
})();
