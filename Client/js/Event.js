$(document).ready(function() {
  function disegna() {
    var str = "";
    Users.getUsers().then(function(data) {
      for(let i= 0; i<data.length; i++) {
        str += "<tr>"
        + "<td>" + data[i].nome + "</td>"
        + "<td>" + data[i].cognome + "</td>"
        + "<td>" + data[i].sesso + "</td>"
        + "<td>" + data[i].eta + "</td>"
        + "<td><button idutente='"+ data[i]._id +"'class='elimina glyphicon glyphicon-trash btn btn-primary'></button></td>"
        + "<td><button idutente='"+ data[i]._id +"'class='modifica glyphicon glyphicon-pencil btn btn-primary'></button></td>"
        + "</tr>"
      }
      $("#tbody").html(str);
      $("#miaform").addClass("hidden");
      $("#aggiornaform").addClass("hidden");

      $('.elimina').click(function() {
        var id = $(this).attr("idutente");
        elimina(id);
      });

      $(".modifica").click(function() {
        var id = $(this).attr("idutente");
        modifica(id);
      });
    }).catch();
  }

  $("#nuovo").click(function() {
    $("#miaform").removeClass("hidden");
    $("#aggiornaform").addClass("hidden");
    $("#miaform").submit(function(event) {
      event.preventDefault();
        var nome = this.nome.value;
        var cognome = this.cognome.value;
        var eta = this.eta.value;
        var sesso = this.sesso.value;
        var nuovo = {
          nome: nome,
          cognome: cognome,
          sesso: sesso,
          eta: eta
        }
        Users.createUser(nuovo);
        //"reset" pulisce la form
        this.reset();
        disegna();
    });
  });

  function elimina(id) {
    Users.deleteUser(id).then(function(data) {
      console.log(data);
      disegna();
    }).catch(function(err) {
      console.log(err);
    });
  }

  function modifica(id) {
    Users.detailUser(id).then(function(user) {
      console.log(user);
      $("#aggiornaform [name=nome]").val(user.nome);
      $("#aggiornaform [name=cognome]").val(user.cognome);
      $("#aggiornaform [name=sesso]").val(user.sesso);
      $("#aggiornaform [name=eta]").val(user.eta);
      $("#aggiornaform").removeClass("hidden");
      $("#miaform").addClass("hidden");
      $("#aggiornaform").submit(function(event) {
        event.preventDefault();
        var nome = this.nome.value;
        var cognome = this.cognome.value;
        var sesso = this.sesso.value;
        var eta = this.eta.value;
        var aggiorna = {
          nome: nome,
          cognome: cognome,
          sesso: sesso,
          eta: eta
        }
        console.log(id);
        Users.updateUser(id, aggiorna).then(function(user) {
          console.log(user);
          disegna();
        }).catch();
        location.reload();
      });
    }).catch(function(err) {
      console.log(err);
    });
  }

  $("#cercaNome").click(function() {
    var nome = $("#inputRicerca").val();
    console.log(nome);
    Users.cercaUsers(nome).then(function(data) {
      console.log(data);
      $("#risultato").html("ho trovato " + data.length + " persone di nome " + nome)
    });
  });

  disegna();
});
