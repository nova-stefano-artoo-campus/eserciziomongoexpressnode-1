var express = require("express");
var path = require("path");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var app = express();
const PORT = 3000;

//Parsers del body
app.use(bodyParser.json());

//Connessione al Database
require("./Config/Database.js")(mongoose);

//Rotte principali
require("./Routes/Routes.js")(app, express, path);

//Start del Server
app.listen(PORT, function() {
  console.log("server connesso su http://localhost:" + PORT);
});
