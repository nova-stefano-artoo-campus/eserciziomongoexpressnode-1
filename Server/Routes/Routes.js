module.exports = function(app, express, path) {
  app.use("/bootstrap", express.static(path.join(__dirname, "..", "..", "node_modules", "bootstrap", "dist")));
  app.use("/jquery", express.static(path.join(__dirname, "..", "..", "node_modules", "jquery", "dist")));
  app.use("/css", express.static(path.join(__dirname, "..", "..", "Client", "css")));
  app.use("/js", express.static(path.join(__dirname, "..", "..", "Client", "js")));

  app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname, "..", "..", "Client", "Index.html"));
  });

  var users = require("./../Users/Index.js");
  app.use("/users", users);
}
