var mongoose = require("mongoose");
var User = require("./Users.model.js");
module.exports = (function() {
  var getUsers = function(req, res) {
    User.find().exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  var createUsers = function(req, res) {
    var utente = req.body;
    var newUtente = new User(utente);
    newUtente.save().then(function(data) {
      res.status(200).send(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  var detailUsers = function(req, res) {
    var id = req.params.id;
    User.findById(id).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  var updateUsers = function(req, res) {
    var id = req.params.id;
    var newData = req.body;
    User.findByIdAndUpdate(id, newData).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  var deleteUsers = function(req, res) {
    var id = req.params.id;
    User.findByIdAndRemove(id).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  var cercaUsers = function(req, res) {
    var cerca = req.query.cerca;
    User.find({
      $or:[{"nome":cerca},{"cognome":cerca}]
    }).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).send(err);
    });
  };

  return {
    getUsers: getUsers,
    createUsers: createUsers,
    detailUsers: detailUsers,
    updateUsers: updateUsers,
    deleteUsers: deleteUsers,
    cercaUsers: cercaUsers
  }
})();
