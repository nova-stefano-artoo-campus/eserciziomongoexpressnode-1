var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema({
  nome: {
    type: String,
    required: [true, 'Devi inserire il nome']
  },

  cognome: {
    type: String,
    required: [true, 'Devi inserire il cognome']
  },

  sesso: String,
  eta: Number
});

var User = mongoose.model("User", userSchema);
module.exports = User;
