var express = require ("express");
var router = express.Router();
var Users = require("./Users.controller.js");

router.get("/", Users.getUsers);

router.get("/id/:id", Users.detailUsers);

router.post("/", Users.createUsers);

router.put("/id/:id", Users.updateUsers);

router.delete("/id/:id", Users.deleteUsers);

router.get("/cerca", Users.cercaUsers);

module.exports = router;
